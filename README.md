# Volatility-check



## Purpose

Script to use with Volatility 2.X that will analyze your memeory dump with various options and save everything to a \<name of your dump\> .volatility.report in the same folder making it more easy to parse and search

/!\ remember to edit the first line to correspond to your volatility install 

To make the script executable

`chmod +x volatility-check.sh`

## Usage 

`./volatility-check <memorydump>`

